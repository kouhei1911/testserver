#!/bin/sh

#古いhtmlファイルを新たなものに置き換えるスクリプトです。
#引数１：プッシュされ屋ファイル名；
#引数２：削除対象のファイルが含まれるディレクトリを指定。

file="$1"

if [ $# -ne 2 ]; then
        echo "実行するには2個の引数が必要です。
        第一引数: プッシュされたファイル名；
        第二引数: 削除対象のファイルが含まれるディレクトリを指定。"
        #exit 1
fi

FILE_LIST=`ls -t $2 | grep "html" ` #grep $fileは改良の必要あり。
count=0

for FILE in $FILE_LIST
do
        ((count++))
        echo "check $FILE"
        if [ $count -gt 1 ]; then
                rm $FILE
                echo "delete $FILE"
        fi
done

echo "finish"

